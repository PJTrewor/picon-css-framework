module.exports = function(grunt) {
	var pageUrl = 'localhost/project'; // URL webu pro browserSync

	// Nastaví správné názvy balíčku pro JIT
	require('jit-grunt')(grunt, {
		sprite: 'grunt-spritesmith'
	});

	grunt.initConfig({
		browserSync: {
			dev: {
				bsFiles: {
					src: ['css/style.css', '_templates/**/*.html', 'images/**/*']
				},
				options: {
					proxy: pageUrl,
					watchTask: true
				}
			}
		},
		concurrent: {
			dev: {
				tasks: ['watch:less', 'watch:scriptsUser', 'watch:scriptsLibs', 'watch:scriptsPlain', 'watch:sprites'],
				options: {
					logConcurrentOutput: true,
					limit: 8
				}
			}
		},
		watch: {
			options: {
				debounceDelay: 250
			},
			less: {
				files: ['assets/less/**/*.less'],
				tasks: 'less:dev'
			},
			scriptsUser: {
				files: ['assets/js/*.js'],
				tasks: 'concat:scriptsUser'
			},
			scriptsLibs: {
				files: ['assets/js/libs/*.js'],
				tasks: 'concat:scriptsLibs'
			},
			scriptsPlain: {
				files: ['assets/js/plain/*.js'],
				tasks: 'copy:scriptsPlain'
			},
			sprites: {
				files: ['assets/sprite/*.png'],
				tasks: 'sprite:dev'
			}
		},
		less: {
			dev: {
				options: {
					sourceMap: true,
					sourceMapURL: 'style.css.map'

				},
				files: {
					'css/style.css': 'assets/less/style.less'
				}
			},
			rel: {
				options: {
					sourceMap: false
				},
				files: {
					'css/style.css': 'assets/less/style.less'
				}
			}
		},
		cssmin: {
			options: {
				aggressiveMerging: false
			},
			rel: {
				files: {
					'css/style.css': 'css/style.css'
				}
			}
		},
		clean: {
			rel: ['css/style.css.map', 'css/style.css', 'js/**/*'],
			js: ['js/**/*'],
			jsmap: ['js/*.map']
		},
		concat: {
			options: {
				sourceMap: true
			},
			scriptsLibs: {
				src: 'assets/js/libs/*.js',
				dest: 'js/libs.js'
			},
			scriptsUser: {
				src: 'assets/js/*.js',
				dest: 'js/main.js'
			}
		},
		uglify: {
			options: {
				banner: '/*! Generated: <%= grunt.template.today("yyyy-mm-dd hh:ss") %> */\n',
				preserveComments: 'some'
			},
			rel: {
				files: {
					'js/libs.js': 'js/libs.js',
					'js/main.js': 'js/main.js'
				}
			}
		},
		copy: {
			scriptsPlain: {
				expand: true,
				flatten: true,
				src: 'assets/js/plain/*',
				dest: 'js/',
				filter: 'isFile'
			}
		},
		sprite: {
			dev: {
				src: 'assets/sprite/*.png',
				retinaSrcFilter: 'assets/sprite/*-2x.png',
				dest: 'images/sprite.png',
				retinaDest: 'images/sprite-2x.png',
				imgPath: '../images/sprite.png',
				retinaImgPath: '../images/sprite-2x.png',
				destCss: 'assets/less/sprite-src.less',
				padding: 5,
				cssFormat: 'less_retina',
				cssOpts: {
					functions: false,
					variableNameTransforms: ['camelize']
				},
				cssVarMap: function(sprite) {
					sprite.name = 'sprite--' + sprite.name;
				}
			}
		}
	});

	grunt.registerTask('default', ['browserSync:dev', 'concurrent:dev']); // synchronizuje browser a watch
	grunt.registerTask('build', ['clean:rel', 'sprite:dev', 'less:rel', 'cssmin:rel', 'concat:scriptsLibs', 'concat:scriptsUser', 'uglify:rel', 'copy:scriptsPlain', 'clean:jsmap']); // kompletní příprava pro produkci
};